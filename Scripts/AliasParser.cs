﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

namespace GuiConsole
{
	public class AliasParser : GuiConsole
	{
		/// <summary>
		/// The type of messages a Alias (Console GUI Window pre-defined method) can send.
		/// </summary>
		public enum TypeOfSendMessage
		{
			SendMessage = 0,
			SendMessageUpwards = 1,
			BroadcastMessage = 2
		}

		/// <summary>
		/// A list of aliases which can be accessed, by calling the alias tag directly in the GUI console window.
		/// Each scene has its own individual list.
		/// </summary>
		public List<AliasObject> AliasList = new List<AliasObject>();

		public int GetAliasObjectCount()
		{
			return AliasList.Count;
		}
		
		public AliasObject GetAliasObject(int index)
		{
			if(index < 0 || index > AliasList.Count)
			{
				Debug.LogError("Invalid index value (" + index +"). No object could be found.");
				return null;
			}
			return AliasList[index];
		}

		/// <summary>
		/// This method checks a string array to see if there is a registered alias in the string array.
		/// A Alias is a pre-defined method call (using GameObject.SendMessage) which are all set in the editor inspector.
		/// There are a number of pre-created aliases as well, which are available at all times.
		/// </summary>
		/// <returns>Return true if there was a valid alias in the string array. Return false otherwise.</returns>
		public bool ParseAliasString(StringParser.ParsedInputParameterObject parameterObject)
		{
			string aliasName = parameterObject.Alias;
			
			if (aliasName == null || aliasName == String.Empty)
				return false;
			
			aliasName = aliasName.ToLower();
			int objectCount = AliasList.Count;
			GameObject currentGameObject = null;
			
			switch (aliasName)
			{
			case "addcomponent":
				parameterObject.InSceneGameObject.AddComponent(parameterObject.GameObjectComponent.ToString());
				return true;
			case "fontsize":
				int currentFontSize = 12;
				currentFontSize = System.Convert.ToInt32(parameterObject.Parameters[0]);
				//ConsoleSkin.box.fontSize = currentFontSize;
				return true;
			case "clear":
				//m_consoleTextList.Clear();
				return true;
			case "close":
				//ActivateScreenConsole = false;
				return true;
			case "exit":
				Application.Quit();
				return true;
			case "loadscene":
				if (parameterObject.Parameters.Length == 1)
					LoadNewScene(parameterObject.Parameters[0]);
				else
					Debug.LogError("Invalid number of inparameters.");
				return true;
			case "displayobject":
				if (parameterObject.Parameters.Length == 2)
				{
					currentGameObject = ParseStringForGameObject(parameterObject.Parameters[0]);
					ToggleVisibleGameObject(currentGameObject, parameterObject.Parameters[1]);
				}
				else
					Debug.LogError("Invalid number of inparameters.");
				return true;
			case "broadcastmessage":
				if (parameterObject.Parameters.Length == 1)
					SendMessageToObject(TypeOfSendMessage.BroadcastMessage, parameterObject.Method, parameterObject.Parameters[0], parameterObject.InSceneGameObject);
				else
					SendMessageToObject(TypeOfSendMessage.BroadcastMessage, parameterObject.Method, null, parameterObject.InSceneGameObject);
				return true;
			case "sendmessageupwards":
				if (parameterObject.Parameters.Length == 1)
					SendMessageToObject(TypeOfSendMessage.SendMessageUpwards, parameterObject.Method, parameterObject.Parameters[0], parameterObject.InSceneGameObject);
				else
					SendMessageToObject(TypeOfSendMessage.SendMessageUpwards, parameterObject.Method, null, parameterObject.InSceneGameObject);
				return true;
			case "sendmessage":
				if (parameterObject.Parameters.Length == 1)
					SendMessageToObject(TypeOfSendMessage.SendMessage, parameterObject.Method, parameterObject.Parameters[0], parameterObject.InSceneGameObject);
				else 
					SendMessageToObject(TypeOfSendMessage.SendMessage, parameterObject.Method, null, parameterObject.InSceneGameObject);
				return true;
			case "help":
				PrintHelpText();
				return true;
			}
			
			for (int i = 0; i < objectCount; ++i)
			{
				string aliasTag = AliasList[i].AliasTag.ToLower();
				if (string.Equals(aliasName, aliasTag) == true)
				{
					string gameObjectName = string.Empty;
					object parameterValue = null;
					
					if (parameterObject.Parameters != null)
					{
						if (parameterObject.Parameters.Length >= 1)
						{
							GameObject foundObject = ParseStringForGameObject(parameterObject.Parameters[0]);
							if (foundObject != null)
								gameObjectName = parameterObject.Parameters[0];
							else
								parameterValue = parameterObject.Parameters[0];
						}
						
						if (parameterObject.Parameters.Length >= 2)
							parameterValue = parameterObject.Parameters[1];
					}
					
					AliasList[i].ParseAliasMethod(gameObjectName, string.Empty, parameterValue);
					return true;
				}
			}
			
			return false;
		}
		
		/// <summary>
		/// Displays a help section in the Console GUI Window.
		/// </summary>
		private void PrintHelpText()
		{
			StringBuilder helpString = new StringBuilder();
			
			helpString.AppendLine();
			helpString.AppendLine("**** Start help section: ****");
			helpString.AppendLine();
			helpString.AppendLine("Access a GameObjects properties or methods from its components by typing in them in the Console window and press return.");
			helpString.AppendLine("Access a alias by typing in the alias name and the optional parametrs. A alias object can be added through the GuiConsoleWindow component.");
			helpString.AppendLine("Please read the ReadMe.txt file in the Log folder, for more information and help.");
			helpString.AppendLine();
			helpString.AppendLine("**** Alias commands: ****");
			helpString.AppendLine();
			helpString.AppendLine("[Alias name]");
			helpString.AppendLine("Starts the defined alias with that name.");
			helpString.AppendLine();
			helpString.AppendLine("[Alias name] [parameter]");
			helpString.AppendLine("Starts the defined alias with that name, but overwrites the default parameter.");
			helpString.AppendLine();
			helpString.AppendLine("[Alias name] [GameObject]");
			helpString.AppendLine("Starts the defined alias with that name, but overwrites the target GameObject.");
			helpString.AppendLine();
			helpString.AppendLine("[Alias name] [GameObject] [parameter]");
			helpString.AppendLine("Starts the defined alias with that name, but overwrites the target GameObject AND the default parameter.");
			helpString.AppendLine();
			helpString.AppendLine("**** predefined commands: ****");
			helpString.AppendLine();
			helpString.AppendLine("SendMessage [GameObject] [Method] [(optional) Object]");
			helpString.AppendLine("Sends a message to a GameObject with a optional parameter variable (object/float/bool/string etc.).");
			helpString.AppendLine();
			helpString.AppendLine("SendMessageUpwards [GameObject] [Method] [(optional) Object]");
			helpString.AppendLine("Sends a message upwards to a GameObject with a optional parameter variable (object/float/bool/string etc.).");
			helpString.AppendLine();
			helpString.AppendLine("BroadcastMessage [GameObject] [Method] [(optional) Object]");
			helpString.AppendLine("Broadcasts a message to a GameObject with a optional parameter variable (object/float/bool/string etc.).");
			helpString.AppendLine();
			helpString.AppendLine("DisplayObject [GameObject] [boolean]");
			helpString.AppendLine("Toggles a object and its children to be either visible (true) or invisible (false).");
			helpString.AppendLine();
			helpString.AppendLine("Clear");
			helpString.AppendLine("Clears the Console GUI window. Log is not affected.");
			helpString.AppendLine();
			helpString.AppendLine("Close");
			helpString.AppendLine("Closes the Console GUI window.");
			helpString.AppendLine();
			helpString.AppendLine("Exit");
			helpString.AppendLine("Closes the Application window.");
			helpString.AppendLine();
			helpString.AppendLine("LoadScene [int]");
			helpString.AppendLine("Loads a scene with the given index value.");
			helpString.AppendLine();
			helpString.AppendLine("LoadScene [string]");
			helpString.AppendLine("Loads a scene with the given name string.");
			helpString.AppendLine();
			helpString.AppendLine("FontSize [int]");
			helpString.AppendLine("Set the Console GUI skin font size. Default value is 12.");
			
			helpString.AppendLine();
			helpString.AppendLine("**** end help section: ****");
			//AddTextToConsole(TypeOfLog.ConsoleDebug, helpString.ToString());
			
		}
		
		/// <summary>
		/// A method which tries to send a GameObject.SendMessage to a object with given parameters.
		/// </summary>
		/// <param name="stringArray">A string array with the parameters in order to send the message further.
		///	\nThe method which will receive the message and its parameter (if any), is gathered from the string array.</param>
		/// <param name="targetObject">The object which will recieve the send message.</param>
		private void SendMessageToObject(TypeOfSendMessage messageType, string method, string parameter, GameObject targetObjectName)
		{
			GameObject targetObject = targetObjectName; // ParseStringForGameObject(targetObjectName);
			if (targetObject == null)
			{
				Debug.LogError("GameObject '" + targetObjectName + "' could not be found.");
				return;
			}
			
			try
			{
				bool booleanResult = false;
				float floatResult = 0.0f;
				int intResult = 0;
				
				if (parameter != null && Boolean.TryParse(parameter.ToString(), out booleanResult) == true)
					switch (messageType)
				{
					case TypeOfSendMessage.SendMessage:
					targetObject.SendMessage(method, booleanResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.SendMessageUpwards:
					targetObject.SendMessageUpwards(method, booleanResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.BroadcastMessage:
					targetObject.BroadcastMessage(method, booleanResult, SendMessageOptions.RequireReceiver);
					break;
				}
				
				else if (parameter != null && int.TryParse(parameter.ToString(), out intResult) == true)
					switch (messageType)
				{
					case TypeOfSendMessage.SendMessage:
					targetObject.SendMessage(method, intResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.SendMessageUpwards:
					targetObject.SendMessageUpwards(method, intResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.BroadcastMessage:
					targetObject.BroadcastMessage(method, intResult, SendMessageOptions.RequireReceiver);
					break;
				}
				else if (parameter != null && float.TryParse(parameter.ToString(), out floatResult) == true)
					switch (messageType)
				{
					case TypeOfSendMessage.SendMessage:
					targetObject.SendMessage(method, floatResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.SendMessageUpwards:
					targetObject.SendMessageUpwards(method, floatResult, SendMessageOptions.RequireReceiver);
					break;
					case TypeOfSendMessage.BroadcastMessage:
					targetObject.BroadcastMessage(method, floatResult, SendMessageOptions.RequireReceiver);
					break;
				}
				else if (parameter != null)
				{
					switch (messageType)
					{
					case TypeOfSendMessage.SendMessage:
						targetObject.SendMessage(method, parameter, SendMessageOptions.RequireReceiver);
						break;
					case TypeOfSendMessage.SendMessageUpwards:
						targetObject.SendMessageUpwards(method, parameter, SendMessageOptions.RequireReceiver);
						break;
					case TypeOfSendMessage.BroadcastMessage:
						targetObject.BroadcastMessage(method, parameter, SendMessageOptions.RequireReceiver);
						break;
					}
				}
				
				else
				{
					switch (messageType)
					{
					case TypeOfSendMessage.SendMessage:
						targetObject.SendMessage(method, SendMessageOptions.RequireReceiver);
						break;
					case TypeOfSendMessage.SendMessageUpwards:
						targetObject.SendMessageUpwards(method, SendMessageOptions.RequireReceiver);
						break;
					case TypeOfSendMessage.BroadcastMessage:
						targetObject.BroadcastMessage(method, SendMessageOptions.RequireReceiver);
						break;
					}
				}
			}
			catch (Exception e)
			{
				GuiConsoleWindow.Instance.AddError(e.Message);
			}
		}
		
		/// <summary>
		/// Makes a object and its children either visible or invisible.
		/// </summary>
		/// <param name="currentGameObject">The GameObject which will be affected.</param>
		/// <param name="parsedStringParameter">A parsed string which should be a boolean. Set to true and the object(s) are visible. Set to false and the object(s) are invisible.</param>
		private void ToggleVisibleGameObject(GameObject currentGameObject, string parsedStringParameter)
		{
			bool booleanResult = false;
			if (parsedStringParameter != null && Boolean.TryParse(parsedStringParameter.ToString(), out booleanResult) == false)
				return;
			
			Renderer currentRenderer = currentGameObject.GetComponent<MeshRenderer>();
			Renderer[] childrenRenderer = currentGameObject.GetComponentsInChildren<MeshRenderer>();
			
			if (currentRenderer != null)
				currentRenderer.enabled = booleanResult;
			
			int objectCount = childrenRenderer.Length;
			for (int i = 0; i < objectCount; ++i)
				childrenRenderer[i].enabled = booleanResult;
		}
		
		/// <summary>
		/// This method tries to load a new level based on the incoming string parameter.
		/// The method tries to parse the strin parameter in order to find if it is a string or a index value and load a level based on the result.
		/// </summary>
		/// <param name="parsedStringName">A valid string which should containing a valid scene name or index value.</param>
		private void LoadNewScene(string parsedStringName)
		{
			try
			{
				int levelIndexValue = 0;
				if (int.TryParse(parsedStringName, out levelIndexValue) == true)
					Application.LoadLevel(levelIndexValue);
				else
					Application.LoadLevel(parsedStringName);
			}
			catch (Exception e)
			{
				GuiConsoleWindow.Instance.AddError(e.Message);
				
			}
		}
	}
}