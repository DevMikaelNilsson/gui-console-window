﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Reflection;
using System.Collections.Generic;

namespace GuiConsole
{
	public class StringParser : GuiConsole 
	{
		/// <summary>
		/// When analyzing a input string entered through the Console GUI Window, all the data is stored as
		/// a struct object. Not all variables are neccessarly filled every time a new struct object is created.
		/// </summary>
		public struct ParsedInputParameterObject
		{
			public GameObject InSceneGameObject;
			public Component GameObjectComponent;
			public string Method;
			public string Alias;
			public string[] Parameters;
		}

		private AliasParser m_aliasParserObject = null;

		public StringParser()
		{
			if(m_aliasParserObject == null)
				m_aliasParserObject = new AliasParser();
		}

		/// <summary>
		/// This method interpets the whole input string and tries to find all the hidden data in the string.
		/// Since all commands needs to be written in a certain way, the method looks through and tries to find all the 
		/// parameters and store them in a struct.
		/// </summary>
		/// <returns>A struct object which contains the result from the input interpetation.</returns>
		public ParsedInputParameterObject InterpetInputString(string inputString)
		{
			ParsedInputParameterObject newParameterStruct = new ParsedInputParameterObject();
			
			string tempInputString = inputString;
			tempInputString = tempInputString.Replace("( ", "(");
			tempInputString = tempInputString.Replace("(", " ");
			tempInputString = tempInputString.Replace(") ", ")");
			tempInputString = tempInputString.Replace(")", " ");
			tempInputString = tempInputString.Replace(", ", ",");
			tempInputString = tempInputString.Replace(",", " ");
			
			string possibleObject = string.Empty;
			string possibleParameter = string.Empty;
			
			if (tempInputString.Contains(" ") == true)
			{
				// To make it easier to find the values in the string, I need to trim it a little bit, removing those chars and strings which are not needed.
				int firstIndex = tempInputString.IndexOf(' ');
				string parameterSubstring = tempInputString.Substring(firstIndex);
				//parameterSubstring = parameterSubstring.Replace(", ", ",");
				possibleParameter = parameterSubstring;
				
				string[] possibleObjectStringResult = tempInputString.Split(new char[] { ' ' });
				possibleObject = possibleObjectStringResult[0];
			}
			
			
			string[] parsedParameterStringArray = null;
			if (possibleObject != string.Empty)
				parsedParameterStringArray = possibleObject.Split(new char[] { '.' });
			else
				parsedParameterStringArray = tempInputString.Split(new char[] { '.' });
			
			if (possibleParameter != string.Empty)
			{
				possibleParameter = possibleParameter.Trim(new char[] { ' ' });
				string[] splittedParameterArray = possibleParameter.Split(new char[] { ',', ' ', '(', ')' });
				newParameterStruct.Parameters = splittedParameterArray;
			}
			
			newParameterStruct.InSceneGameObject = ParseStringForGameObject(parsedParameterStringArray[0]);
			if (newParameterStruct.InSceneGameObject != null)
			{
				newParameterStruct.GameObjectComponent = newParameterStruct.InSceneGameObject.GetComponent(parsedParameterStringArray[1]);
				
				if (newParameterStruct.GameObjectComponent != null)
					newParameterStruct.Method = parsedParameterStringArray[2];
				else
					newParameterStruct.Method = parsedParameterStringArray[1];
			}
			else
			{
				if (possibleObject != string.Empty)
				{
					newParameterStruct.Alias = possibleObject;
					string []splittedParameterString = possibleParameter.Split(new char[] { ' ' });
					
					newParameterStruct.InSceneGameObject = ParseStringForGameObject(splittedParameterString[0]);
					
					if (splittedParameterString.Length >= 2)
						newParameterStruct.Method = splittedParameterString[1];
					
					if(splittedParameterString.Length == 3)
					{
						newParameterStruct.Parameters = new string[1];
						newParameterStruct.Parameters[0] = splittedParameterString[2];
					}
				}
				else
					newParameterStruct.Alias = tempInputString;
			}
			
			// Uncomment below for debuggig purposes.
			/*
			Debug.Log(newParameterStruct.InSceneGameObject);
			Debug.Log(newParameterStruct.GameObjectComponent);
			Debug.Log(newParameterStruct.Method);
			Debug.Log(newParameterStruct.Alias);
			if (newParameterStruct.Parameters != null)
				for (int i = 0; i < newParameterStruct.Parameters.Length; ++i)
					Debug.Log(newParameterStruct.Parameters[i]);
			*/
			return newParameterStruct;
		}

		/// <summary>
		/// Method takes the existing string and tries to parse the information, and to display all possible matching properties/methods.
		/// </summary>
		/// <param name="yPosition">The position for the extra popup window which will display all possible matches.</param>
		public string DisplayPossibleMatch(float yPosition, string inputString, List<string> m_predefinedAliasList)
		{
			string[] parsedInparameterString = inputString.Split(new char[] { ',', ' ', '(', ')' });
			string[] parsedObjectAndMethod = parsedInparameterString[0].Split('.');
			
			if (parsedObjectAndMethod.Length <= 0)
				return string.Empty;
			
			GameObject currentObject = ParseStringForGameObject(parsedObjectAndMethod[0]);
			StringBuilder stringList = new StringBuilder();
			if (currentObject != null)
			{
				if (parsedObjectAndMethod.Length < 2)
					return string.Empty;
				
				MethodInfo[] assemblyMethods = GetAssemblyMethods(currentObject);
				
				
				for (int i = 0; i < assemblyMethods.Length; ++i)
				{
					if (assemblyMethods[i].ToString().Contains(parsedObjectAndMethod[1]) == true)
						stringList.AppendLine(assemblyMethods[i].ToString());
				}
				
				if (parsedObjectAndMethod.Length == 2)
				{
					System.Type objectType = currentObject.GetType();
					PropertyInfo[] propertyArray = objectType.GetProperties();
					
					for (int i = 0; i < propertyArray.Length; ++i)
					{
						if (propertyArray[i].ToString().Contains(parsedObjectAndMethod[1]) == true)
							stringList.AppendLine(propertyArray[i].ToString());
					}
				}
				else
				{
					Component currentComponent = currentObject.GetComponent(parsedObjectAndMethod[1]);
					if (currentComponent != null)
					{
						System.Type componentType = currentComponent.GetType();
						MethodInfo[] methodArray = componentType.GetMethods();
						
						for (int i = 0; i < methodArray.Length; ++i)
						{
							if (methodArray[i].ToString().Contains(parsedObjectAndMethod[2]) == true)
								stringList.AppendLine(methodArray[i].ToString());
						}
					}
				}
			}
			else
			{
				if (parsedObjectAndMethod[0] == string.Empty)
					return null;
				
				GameObject[] objectList = GetGameObjectList(parsedObjectAndMethod[0]);
				int objectCount = objectList.Length;
				for (int i = 0; i < objectCount; ++i)
					stringList.AppendLine(objectList[i].name);
				
				objectCount = m_aliasParserObject.GetAliasObjectCount();
				for (int i = 0; i < objectCount; ++i)
				{
					AliasObject currentAliasObject = m_aliasParserObject.GetAliasObject(i);
					if(currentAliasObject != null)
					{
						string aliasTag = currentAliasObject.AliasTag;
						if (aliasTag.Contains(parsedObjectAndMethod[0]) == true)
							stringList.AppendLine("[Alias Tag] " + currentAliasObject.AliasTag);
					}
				}
				
				objectCount = m_predefinedAliasList.Count;
				for(int i = 0; i < objectCount; ++i)
				{
					if(m_predefinedAliasList[i].Contains(parsedObjectAndMethod[0]) == true)
					{
						stringList.Append("[Predefined] ");
						stringList.AppendLine(m_predefinedAliasList[i]);
					}
				}
				
			}

			return stringList.ToString();
		}

		/// <summary>
		/// Adds all methods which is referenced to the current object.
		/// </summary>
		/// <param name="currentObject">The current object which the methods will be collected from.</param>
		/// <returns>An MethodInfo array with all added methods.</returns>
		private MethodInfo[] GetAssemblyMethodArray(GameObject currentObject)
		{
			List<MethodInfo> MethodList = new List<MethodInfo>();
			Component[] objectComponentList = currentObject.GetComponents(typeof(MonoBehaviour));
			Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			int componentCount = objectComponentList.Length;
			int assemblyCount = referencedAssemblies.Length;

			for (int componentIndex = 0; componentIndex < componentCount; ++componentIndex)
			{
				for (int assemblyIndex = 0; assemblyIndex < assemblyCount; ++assemblyIndex)
				{
					System.Type type = referencedAssemblies[assemblyIndex].GetType(objectComponentList[componentIndex].GetType().ToString());

					if( type != null )
					{
						System.Reflection.MethodInfo []methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
						for (int methodIterator = 0; methodIterator < methods.Length; ++methodIterator)
							MethodList.Add(methods[methodIterator]);
					}
				}
			}

			return MethodList.ToArray();
		}

		/// <summary>
		/// Retrieves a filtered array of GameObjects.
		/// Only the GameObjects which fits the inparameter string will be added to the array.
		/// </summary>
		/// <param name="filterGameObjectString">The name of the GameObject must contain this string in order to be added to the array.</param>
		/// <returns>A array of valid GameObjects.</returns>
		private GameObject[] GetGameObjectList(string filterGameObjectString)
		{
			List<GameObject> availableGamObjects = new List<GameObject>();
			GameObject[] allObjects = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
			int objectCount = allObjects.Length;
			for (int i = 0; i < objectCount; ++i)
			{
				if (allObjects[i].activeInHierarchy == false)
					continue;
				else if (allObjects[i].name.Contains(filterGameObjectString) == true)
					availableGamObjects.Add(allObjects[i]);
			}
			
			return availableGamObjects.ToArray();
		}

		/// <summary>
		/// Parses the parameter values. the parameter values can contain any kind of object,
		/// normally a string, boolean or a float.
		/// This method is called by the GUI console window when there has been a string input.
		/// The method checks the parameters and tries to convert them to the proper variable before returned to the 
		/// main method for further calculations.
		/// </summary>
		/// <param name="parameterValue">The array of parameters.</param>
		/// <returns>A array of objects. These objects can be a string, boolean or a float.</returns>
		public object[] ParseParameterValue(string[] parameterValue)
		{
			if (parameterValue == null)
				return parameterValue;
			
			List<object> parsedParameterList = new List<object>(); // new object[parameterValue.Length];
			List<float> floatList = new List<float>();
			
			int intResult = 0;
			float floatResult = 0.0f;
			bool boolResult = false;
			
			for (int i = 0; i < (parameterValue.Length); ++i)
			{
				if (parameterValue[i] == string.Empty)
					continue;
				
				else if (int.TryParse(parameterValue[i], out intResult) == true)
				{
					parsedParameterList.Add(intResult);
				}
				else if (float.TryParse(parameterValue[i], out floatResult) == true)
				{
					floatList.Add(floatResult);
					parsedParameterList.Add(floatResult);
				}
				else if (bool.TryParse(parameterValue[i], out boolResult) == true)
					parsedParameterList.Add(boolResult);
				else
					parsedParameterList.Add(parameterValue[i]);
			}
			
			if (floatList.Count >= 2)
				return ParseParameterFloatValue(floatList.ToArray());
			else
				return parsedParameterList.ToArray();
		}
		
		/// <summary>
		/// The method takes a array of floats and checks if they should be merged into a vector or a Quaternion.
		/// </summary>
		/// <param name="parameterValue">The object array with floats.</param>
		/// <returns>Either a vector or a Quaternion, stored as a object in a object array. If the method is unsuccesfull, then the method returns null.</returns>
		public object[] ParseParameterFloatValue(float[] parameterValue)
		{
			object[] vectorParameter = new object[1];
			switch (parameterValue.Length)
			{
			case 2:
				vectorParameter[0] = new Vector2(parameterValue[0], parameterValue[1]);
				break;
			case 3:
				vectorParameter[0] = new Vector3(parameterValue[0], parameterValue[1], parameterValue[2]);
				break;
			case 4:
				vectorParameter[0] = new Quaternion(parameterValue[0], parameterValue[1], parameterValue[2], parameterValue[3]);
				break;
			default:
				return null;
			}
			
			return vectorParameter;
		}
	}
}
