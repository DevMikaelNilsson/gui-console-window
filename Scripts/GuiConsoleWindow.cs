﻿/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;

namespace GuiConsole
{
	[AddComponentMenu("GuiConsoleWindow/GuiConsoleWindow")]
	public class GuiConsoleWindow : MonoBehaviour 
	{

		public KeyCode ActivateConsole = KeyCode.Tab;
				
		/// <summary>
		/// The current version as a string.
		/// </summary>
		private static GuiConsoleWindow m_currentInstance = null;
				
		/// <summary>
		/// A GUISkin for the GUI console window.
		/// </summary>
		public GUISkin ConsoleSkin = null;
		

		
		/// <summary>
		/// A input string for the GUI console window.
		/// When a user types in something in the GUI console window, it is stored in this string.
		/// </summary>
		private string inputString = string.Empty;
		
		/// <summary>
		/// A input string for the GUI console window.
		/// When the inputString is parsed by the GuiConsoleWindow, the previous value is saved in this string,
		/// before the inputString is resetted and emptied.
		/// </summary>
		private string previousInputString = string.Empty;
		
		/// <summary>
		/// A static list for the GUI console window.
		/// </summary>
		private static List<string> m_consoleTextList = new List<string>();
		
		private static List<string> m_predefinedAliasList = new List<string>();
		
		/// <summary>
		/// A scroll position value for the GUI console window.
		/// </summary>
		private Vector2 m_consoleWindowScrollPosition = Vector2.zero;
		
		/// <summary>
		/// When the Console GUI is activated, a GUI object needs to have the focus. 
		/// If a GUI object has recieved the focus, then this flag will be set to true.
		/// This flag is resetted every time the Console GUI is hidden.
		/// </summary>
		private bool m_hasConsoleGuiObjectFocus = false;
		
		/// <summary>
		/// This boolean flag allows the Console GUI to gain focus to a GUI object.
		/// When the Console GUI is activated, a GUI object needs to have the focus. 
		/// In order to avoid that the keyboard input which activates the Console GUI is added to the
		/// focused GUI object, a short delay is made before this boolean is set to true.
		/// This flag is resetted when a GUI object has recieved focus, or if a GUI object already has the focus.
		/// </summary>
		private bool m_allowCheckForGuiObjectFocus = false;
		
		/// <summary>
		/// Flag to display a simple "welcome message" in the Console GUI Window system.
		/// This message should only be displayed once per game.
		/// </summary>
		private static bool m_hasShownIntroductionString = false;
		
		/// <summary>
		/// A name for the Textfield GUI object in the Console GUI.
		/// </summary>
		private const string m_guiConsoleTextFieldObject = "ConsoleTextField";
		
		/// <summary>
		/// A name for the Clear all GUI button in the Console GUI.
		/// </summary>
		private const string m_guiConsoleClearAllButtonObject = "ClearAllButton";
		
		/// <summary>
		/// A name for the repeat command GUI button in the Console GUI.
		/// </summary>
		private const string m_guiConsoleRepeatButtonObject = "RepeatCommandButton";
		
		/// <summary>
		/// A name for text box GUI object in the Console GUI.
		/// </summary>
		private const string m_guiConsoleTextBoxObject = "TextBox";
		
		/// <summary>
		/// If set to true, the mnGuiConsoleWindow will also add all log texts to the screen through the
		/// LogToScreenManager class. This boolean works along with the AlwaysWriteLogToScreen.
		/// </summary>
		public static bool ActivateScreenConsole = false;
		
		/// <summary>
		/// Available types of logs the GuiConsoleWindow can handle.
		/// </summary>
		public enum TypeOfLog
		{
			/// <summary>
			/// Normal, non critical log. Meant for pure informative log texts.
			/// </summary>
			Info = 0,
			
			/// <summary>
			/// Semi critical log. Should be used if something has gone wrong, but will not affect
			/// the gameplay/application in a bigger sense.
			/// </summary>
			Warning = 1,
			
			/// <summary>
			/// Critical log. Should be used if someting has gone wrong that might affect the gameplay/application in any degree.
			/// </summary>
			Error = 2,
			
			/// <summary>
			/// Non-critical log. This is mainly made for the GUI console window to display simple information.
			/// </summary>
			ConsoleDebug = 3,
			
			/// <summary>
			/// Non-critical log. This is mainly made for the GUI console window to display a returning value from a method call.
			/// </summary>
			Value = 4
		}

		private CommandParser m_commandParserObject = null;
		private AliasParser m_aliasParserObject = null;
		private StringParser m_stringParserObject = null;
				
		/// <summary>
		/// Returns the active instance. If there is no active instance, a new instance will be 
		/// automatically created and returned.
		/// </summary>
		public static GuiConsoleWindow Instance
		{
			get
			{
				if (m_currentInstance == null)
				{
					m_currentInstance = (GuiConsoleWindow)FindObjectOfType(typeof(GuiConsoleWindow));
				}
				
				// If it is still null, create a new instance
				if (m_currentInstance == null)
				{
					GameObject obj = new GameObject("GuiConsoleWindow");
					m_currentInstance = (GuiConsoleWindow)obj.AddComponent(typeof(GuiConsoleWindow));
				}
				
				return m_currentInstance;
			}
		}
				
		/// <summary>
		/// When the object is enabled.
		/// </summary>
		void OnEnable()
		{
			if(m_commandParserObject == null)
				m_commandParserObject = new CommandParser();
			if(m_aliasParserObject == null)
				m_aliasParserObject = new AliasParser();
			if(m_stringParserObject == null)
				m_stringParserObject = new StringParser();

			LoadPredefinedAliasToList();
			Application.RegisterLogCallback(HandleLog);
			
			if (m_hasShownIntroductionString == false)
			{
				m_hasShownIntroductionString = true;
				StringBuilder introString = new StringBuilder();
				introString.AppendLine("**** GuiConsoleWindow Console GUI ****");
				introString.AppendLine("Type help for help and information.");
				introString.AppendLine();
				m_consoleTextList.Add(introString.ToString());
			}
		}
		
		/// <summary>
		/// When the object is disabled.
		/// </summary>
		void OnDisable()
		{
			Application.RegisterLogCallback(null);
		}
		
		/// <summary>
		/// This method is a callback for the Unity log functionality.
		/// When there is a new message for the debug log, it also sends the message to this method
		/// and the method will add it to the GuiConsoleWindow logs.
		/// </summary>
		/// <param name="logString">The message string.</param>
		/// <param name="stackTrace">The stack trace.</param>
		/// <param name="type">The type of log.</param>
		private void HandleLog(string logString, string stackTrace, LogType type)
		{
			switch (type)
			{
			case LogType.Log:
				AddInformation(logString);
				break;
			case LogType.Warning:
				AddWarning(logString);
				break;
			default:
				AddError(logString);
				break;
			}
		}
		
		/// <summary>
		/// A Unity method which is called after the normal update methods.
		/// Use this if the code in the method is not critical for the gameplay.
		/// </summary>
		void LateUpdate()
		{
			if (Input.GetKeyDown(ActivateConsole) == true)
			{
				if (ActivateScreenConsole == false)
					ActivateScreenConsole = true;
				else
					ActivateScreenConsole = false;
			}
			
			
			if (ActivateScreenConsole == false)
			{
				inputString = string.Empty;
				m_hasConsoleGuiObjectFocus = false;
			}
		}
		
		/// <summary>
		/// Unity function.
		/// This function will be automatically called several times a frame.
		/// </summary>
		private void OnGUI()
		{
			if (ActivateScreenConsole == true)
			{
				if (ConsoleSkin == null)
				{
					ConsoleSkin = GUI.skin;
					SetupCustomGuiSkin();
				}
				
				GUI.skin = ConsoleSkin;
				int objectCount = m_consoleTextList.Count;
				StringBuilder stringList = new StringBuilder();
				for (int i = 0; i < objectCount; ++i)
					stringList.Append(m_consoleTextList[i]);
				
				float heightSize = CalculateBoxHeight("Box", stringList.ToString(), (Screen.height / 2.0f));
				
				GUI.SetNextControlName(m_guiConsoleClearAllButtonObject);
				if (GUI.Button(new Rect(0.0f, 0.0f, (Screen.width / 2.0f), 25.0f), "Clear All") == true)
				{
					m_consoleTextList.Clear();
					return;
				}
				
				GUI.SetNextControlName(m_guiConsoleRepeatButtonObject);
				if (GUI.Button(new Rect((Screen.width / 2.0f), 0.0f, (Screen.width / 2.0f), 25.0f), "Repeat last command") == true)
				{
					inputString = previousInputString;
					return;
				}
				
				m_consoleWindowScrollPosition = GUI.BeginScrollView(new Rect(0, 25.0f, Screen.width, (Screen.height / 2.0f)), m_consoleWindowScrollPosition, new Rect(0, 25.0f, 0, heightSize));
				GUI.SetNextControlName(m_guiConsoleTextBoxObject);
				GUI.Box(new Rect(0.0f, 25.0f, Screen.width, heightSize), stringList.ToString());
				GUI.EndScrollView();
				
				
				if (Event.current.type == EventType.KeyDown && Event.current.character == '\n')
				{
					m_commandParserObject.ParseString(inputString);
					ResetInputStringAndUpdateConsoleWindows();
				}
				else
				{
					GUI.SetNextControlName(m_guiConsoleTextFieldObject);
					float yPosition = ((Screen.height / 2.0f) + 25.0f);
					inputString = GUI.TextField(new Rect(0.0f, yPosition, Screen.width, 25.0f), inputString);
					string listOfMatches = m_stringParserObject.DisplayPossibleMatch(yPosition, inputString, m_predefinedAliasList);

					if(listOfMatches != string.Empty)
					{
						float ySize = CalculateBoxHeight("Window", listOfMatches, 0.0f);
						float maxYSize = (Screen.height - (yPosition + 55.0f));
						ySize = Mathf.Clamp(ySize, 0.0f, maxYSize);
						GUI.Box(new Rect(0.0f, (yPosition + 55.0f), Screen.width, ySize), listOfMatches, ConsoleSkin.window);
					}


				}
				
				CheckForFocus();
				if (m_hasConsoleGuiObjectFocus == false)
					StartCoroutine(DelayUntilAllowForFocusCheck());
				
			}
		}
		
		/// <summary>
		/// Method which checks if a given GUI object has allowed to have focus.
		/// If the GUI object is allowed to have focus, then set the focus to that GUI object.
		/// </summary>
		private void CheckForFocus()
		{
			if (m_allowCheckForGuiObjectFocus == false)
				return;
			else
				m_allowCheckForGuiObjectFocus = false;
			
			if (GUI.GetNameOfFocusedControl() == string.Empty)
			{
				GUI.FocusControl(m_guiConsoleTextFieldObject);
				inputString = string.Empty;
				m_hasConsoleGuiObjectFocus = true;
			}
		}
		
		/// <summary>
		/// Coroutine method which activates a flag in order to allow a GUI object to gain GUI input focus.
		/// </summary>
		/// <returns></returns>
		private IEnumerator DelayUntilAllowForFocusCheck()
		{
			yield return new WaitForSeconds(0.5f);
			m_allowCheckForGuiObjectFocus = true;
		}
		
		/// <summary>
		/// Writes a information line to the log.
		/// Information is a normal, non critical log. Meant for pure informative texts.
		/// </summary>
		/// <param name="text">The information text which will be written into the log.</param>
		public void AddInformation(string text)
		{
			AddTextToConsole(TypeOfLog.Info, text);
			AddTextToConsole(TypeOfLog.Info, text);
		}
		
		/// <summary>
		/// Writes a warning line to the log.
		/// Warning is a semi critical log. Should be used if something has gone wrong, but will not affect
		/// the gameplay/application in a bigger sense.
		/// </summary>
		/// <param name="text">The warning text which will be written into the log.</param>
		public void AddWarning(string text)
		{
			AddTextToConsole(TypeOfLog.Warning, text);
			AddTextToConsole(TypeOfLog.Warning, text);
		}
		
		/// <summary>
		/// Writes a error line to the log.
		/// Error is a critical log. Should be used if someting has gone wrong that might affect the gameplay/application in any degree.
		/// </summary>
		/// <param name="text">The error text which will be written into the log.</param>
		public void AddError(string text)
		{
			AddTextToConsole(TypeOfLog.Error, text);
			AddTextToConsole(TypeOfLog.Error, text);
		}
		
		/// <summary>
		/// Writes a return value line in the log.
		/// A return value is a none-critical log. Purely for information purposes.
		/// </summary>
		/// <param name="text">The error text which will be written into the log.</param>
		public void AddReturnValue(string text)
		{
			AddTextToConsole(TypeOfLog.Value, text);
			AddTextToConsole(TypeOfLog.Value, text);
		}
		
		/// <summary>
		/// Writes a line in the console.
		/// A console line is a none-critical log. Purely for information purposes.
		/// </summary>
		/// <param name="text">The error text which will be written into the log.</param>
		public void AddConsoleDebug(string text)
		{
			AddTextToConsole(TypeOfLog.ConsoleDebug, text);
			AddTextToConsole(TypeOfLog.ConsoleDebug, text);
		}
		
		/// <summary>
		/// Writes to the Unity debug log.
		/// </summary>
		/// <param name="messageType">Type of message to send.</param>
		/// <param name="text">Log text to send.</param>
		private void WriteToDebugLog(TypeOfLog messageType, string text)
		{
			switch (messageType)
			{
			case TypeOfLog.Info:
				UnityEngine.Debug.Log(text);
				break;
			case TypeOfLog.Warning:
				UnityEngine.Debug.LogWarning(text);
				break;
			case TypeOfLog.Error:
				UnityEngine.Debug.LogError(text);
				break;
			default:
				StringBuilder newString = new StringBuilder();
				newString.Append("[");
				newString.Append(messageType.ToString());
				newString.Append("] ");
				newString.Append(text);
				UnityEngine.Debug.Log(newString.ToString());
				break;
			}
			return;
		}
		
		/// <summary>
		/// If there isn't a pre-defined GUISkin set to the GuiConsoleWindow object
		/// when using the GUI console window, a new GUISkin will be automatically created
		/// with some basic setup properties.
		/// </summary>
		private void SetupCustomGuiSkin()
		{
			ConsoleSkin.box.alignment = TextAnchor.LowerLeft;
			ConsoleSkin.box.fontStyle = FontStyle.Normal;
			ConsoleSkin.box.wordWrap = true;
			
			ConsoleSkin.window.alignment = TextAnchor.UpperLeft;
			ConsoleSkin.window.fontStyle = FontStyle.Normal;
			ConsoleSkin.window.wordWrap = true;
		}

					
		/// <summary>
		/// Resets the input string and stores the last command.
		/// The script also recalculates the actual size of the Console GUI window.
		/// </summary>
		private void ResetInputStringAndUpdateConsoleWindows()
		{
			previousInputString = inputString;
			inputString = string.Empty;
			
			int objectCount = m_consoleTextList.Count;
			StringBuilder stringList = new StringBuilder();
			for (int i = 0; i < objectCount; ++i)
				stringList.Append(m_consoleTextList[i]);
			
			m_consoleWindowScrollPosition.y = CalculateBoxHeight("Box", stringList.ToString(), (Screen.height / 2.0f));
		}
		

		
		
		/// <summary>
		/// Loads a list of all predefined aliases which are available for usage at any time.
		/// Each element in the list also show its inparameters, if any, to make it easier to use them properly.
		/// </summary>
		private void LoadPredefinedAliasToList()
		{
			if(m_predefinedAliasList.Count != 0)
				return;
			
			m_predefinedAliasList.Add("AddComponent [GameObject] [Component]");
			m_predefinedAliasList.Add("FontSize [int32]");
			m_predefinedAliasList.Add("Clear");
			m_predefinedAliasList.Add("Close");
			m_predefinedAliasList.Add("Exit");
			m_predefinedAliasList.Add("LoadScene [string]");
			m_predefinedAliasList.Add("LoadScene [int32]");
			m_predefinedAliasList.Add("DisplayObject [GameObject] [boolean]");
			m_predefinedAliasList.Add("BroadcastMessage [GameObject] [(method)string] [(optional)object]");
			m_predefinedAliasList.Add("SendmessageUpwards [GameObject] [(method)string] [(optional)object]");
			m_predefinedAliasList.Add("Sendmessage [GameObject] [(method)string] [(optional)object]");
			m_predefinedAliasList.Add("Help");
		}
		

		

		
		
		

		
		/// <summary>
		/// Calculates the current height of the GUI console window.
		/// The height is calculated based on how many rows the console window currently has.
		/// </summary>
		/// <returns>The proper height value in the y-axis.</returns>
		private float CalculateBoxHeight(string styleString, string stringList, float minHeightSize)
		{
			GUIStyle currentGuiStyle = ConsoleSkin.GetStyle(styleString);
			GUIContent currentContent = new GUIContent(stringList);
			float currentSize = currentGuiStyle.CalcHeight(currentContent, Screen.width);
			return Mathf.Clamp(currentSize, minHeightSize, currentSize);
		}



		
		/// <summary>
		/// Adds a message to the GUI console window. 
		/// The method automatically adds some information to the string and displays it in the GUI window.
		/// </summary>
		/// <param name="logType">The type of log to display.</param>
		/// <param name="message">The actual console message which will be displayed.</param>
		private void AddTextToConsole(TypeOfLog logType, string message)
		{
			StringBuilder finalMessage = new StringBuilder();
			finalMessage.Append(DateTime.Now.ToString(("HH.mm.ss")));
			finalMessage.Append(" |");
			finalMessage.Append(logType.ToString());
			finalMessage.Append("| ");
			finalMessage.Append(message);
			finalMessage.Append("\n");
			m_consoleTextList.Add(finalMessage.ToString());
		}
		

		

		




	}
}
