﻿using UnityEngine;
using System.Collections;
using System;

namespace GuiConsole
{
	public class GuiConsole 
	{
		/// <summary>
		/// Parses a string and makes checks to see if the string is actually a gameobject which is active in the current scene.
		/// If the string is actually a name of a valid gameobject, the gameobject will be returned.
		/// </summary>
		/// <param name="variableString">The string to parse. The string is NOT case sensitive.</param>
		/// <returns>A valid gameobject if the method finds one based on the inparameter string. Returns null otherwise.</returns>
		protected GameObject ParseStringForGameObject(string variableString)
		{
			GameObject[] allObjects = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
			int objectCount = allObjects.Length;
			for (int i = 0; i < objectCount; ++i)
			{
				if (allObjects[i].activeInHierarchy == false)
					continue;
				
				if (string.Equals(allObjects[i].name, variableString) == true)
					return allObjects[i];
			}
			
			return null;
		}
	}
}