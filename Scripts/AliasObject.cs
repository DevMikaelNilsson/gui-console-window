﻿/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using System;

namespace GuiConsole
{
	/// <summary>
	/// A Class which is used to store user defined aliases which are used in the GUI Console window.
	/// A Alias object is simply a GameObject.SendMessage class which sends a SendMessage to a object, when the parameters are properly made.
	/// This is a powerfull method to debug objects in a scene in realtime without the need to do certain keyboard combinations etc.
	/// </summary>
	[System.Serializable]
	public class AliasObject : GuiConsole
	{
		public string AliasTag = string.Empty;
		public GameObject Object = null;
		public SendMessageOptions MessageType = SendMessageOptions.RequireReceiver;
		public string Method = string.Empty;
		public string Parameter = string.Empty;
		
		private object m_internalParameter = null;
		
		/// <summary>
		/// Method parses a alias method. The method finds the object, and sends a message to the given method with a optional parameter value.
		/// </summary>
		/// <param name="objectName">The object name. If the default object should be used, set this value to null.</param>
		/// <param name="methodName">Method name. If the default method should be used, set this value to null.</param>
		/// <param name="parameter">Parameter object. If default value should be used, set this value to null.</param>
		public void ParseAliasMethod(string objectName, string methodName, object parameter)
		{
			GameObject currentGameObject = Object;
			string currentMethod = Method;
			object currentParameter = Parameter;
			
			if (objectName != string.Empty)
			{
				GameObject[] allObjects = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
				int objectCount = allObjects.Length;
				bool foundObject = false;
				for (int i = 0; i < objectCount; ++i)
				{
					if (allObjects[i].activeInHierarchy == false)
						continue;
					
					if (string.Equals(allObjects[i].name, objectName) == true)
					{
						currentGameObject = allObjects[i];
						foundObject = true;
						break;
					}
				}
				
				if (foundObject == false)
				{
					GuiConsoleWindow.Instance.AddError("GameObject '" + objectName + "' could not be found.");
					return;
				}
			}
			if (methodName != string.Empty)
				currentMethod = methodName;
			if (parameter != null)
				currentParameter = parameter;
			
			ParseMethod(currentGameObject, currentMethod, currentParameter);
		}
		
		/// <summary>
		/// Method parses a alias method. The method uses the default values (which can be set through the editor inspector)
		/// and sends a object message if applicable.
		/// </summary>
		public void ParseAliasMethod()
		{
			if (Parameter != string.Empty)
				m_internalParameter = Parameter;
			
			ParseMethod(Object, Method, m_internalParameter);
		}
		
		/// <summary>
		/// Internal method which puts all parameters together and tries to send a GameObject.SendMessage using the available parameters.
		/// </summary>
		/// <param name="objectName">The object which the message will be send to.</param>
		/// <param name="methodName">The method which will be called..</param>
		/// <param name="parameter">Optional parameter object which can be send to the method.</param>
		private void ParseMethod(GameObject currentObject, string methodName, object parameter)
		{
			if (currentObject != null)
			{
				try
				{
					bool booleanResult = false;
					float floatResult = 0.0f;
					int intResult = 0;
					
					if (parameter != null && Boolean.TryParse(parameter.ToString(), out booleanResult) == true)
						currentObject.SendMessage(methodName, booleanResult, SendMessageOptions.RequireReceiver);
					else if (parameter != null && int.TryParse(parameter.ToString(), out intResult) == true)
						currentObject.SendMessage(methodName, intResult, SendMessageOptions.RequireReceiver);
					else if (parameter != null && float.TryParse(parameter.ToString(), out floatResult) == true)
						currentObject.SendMessage(methodName, floatResult, SendMessageOptions.RequireReceiver);
					else if (parameter != null)
						currentObject.SendMessage(methodName, parameter, SendMessageOptions.RequireReceiver);
					else
					{
						currentObject.SendMessage(methodName, SendMessageOptions.RequireReceiver);
					}
				}
				catch (Exception e)
				{
					GuiConsoleWindow.Instance.AddError(e.Message);
				}
			}
			else
				GuiConsoleWindow.Instance.AddError("Gameobject is missing or invalid.");
		}
	}
}