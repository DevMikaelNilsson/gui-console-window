﻿/*************************************************
The MIT License (MIT)

Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace GuiConsole
{
	public class CommandParser : GuiConsole
	{
		private AliasParser m_aliasParserObject = null;
		private StringParser m_stringParserObject = null;

		public CommandParser()
		{
			if(m_aliasParserObject == null)
				m_aliasParserObject = new AliasParser();
			if(m_stringParserObject == null)
				m_stringParserObject = new StringParser();
		}

		/// <summary>
		/// This method parses the input string from the GUI console window.
		/// The method tries to get out as much information as possible with a array of different characters,
		/// splitting the full string into smaller string arrays which can be handled by other methods.
		/// The input string can either be considered to be a method call (get or set) or a simple console debug call (showing up in the console window).
		/// </summary>
		public void ParseString(string inputString)
		{
			StringParser.ParsedInputParameterObject newParameterStruct = m_stringParserObject.InterpetInputString(inputString);
			
			if (inputString == string.Empty)
				return;

			if (m_aliasParserObject.ParseAliasString(newParameterStruct) == true)
				return;
			else if (ParseProperty(newParameterStruct) == true)
				return;
			else if (ParseAssemblyMethod(newParameterStruct) == true)
				return;
			else
				ParseMethod(newParameterStruct);

		}

		/// <summary>
		/// Parses the information given and tries to send a method call (invoke) through the assemblies.
		/// </summary>
		/// <param name="parameterObject">A struct object with all gathered information.</param>
		/// <returns>True if the method is successfull in sending (invoke) a method call. Return false otherwise.</returns>
		private bool ParseAssemblyMethod(StringParser.ParsedInputParameterObject parameterObject)
		{
			string methodName = parameterObject.Method;
			object[] inparameterValue = m_stringParserObject.ParseParameterValue(parameterObject.Parameters);
			GameObject currentObject = parameterObject.InSceneGameObject;
			
			if (currentObject != null)
			{
				MethodInfo[] ListOFMethods = m_stringParserObject.GetAssemblyMethods(currentObject);
				for (int i = 0; i < ListOFMethods.Length; ++i)
				{
					string[] splittedString = ListOFMethods[i].ToString().Split(new Char[] { ' ', '(' });
					
					if (splittedString[1].Equals(methodName) == true)
					{
						SendAssemblyMethod(currentObject, methodName, inparameterValue);
						return true;
					}
				}
			}
			
			return false;
		}

		/// <summary>
		/// Sends (invoke) a method found through the assemblies.
		/// </summary>
		/// <param name="currentObject">The object the method is connected to.</param>
		/// <param name="methodName">The name of the method which needs to be found and invoked.</param>
		/// <param name="inparameters">Optional inparameters for the method.</param>
		private void SendAssemblyMethod(GameObject currentObject, string methodName, object[] inparameters)
		{
			Component[] objectComponentList = currentObject.GetComponents(typeof(MonoBehaviour));
			int componentObjectCount = objectComponentList.Length;
			
			Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			int referenceAssemblyObjectCount = referencedAssemblies.Length;
			
			for (int i = 0; i < componentObjectCount; ++i)
			{
				string[] currentComponent = objectComponentList[i].ToString().Split(new char[] { '(', ')' });
				for (int assemblyObjectIterator = 0; assemblyObjectIterator < referenceAssemblyObjectCount; ++assemblyObjectIterator)
				{
					System.Type type = referencedAssemblies[assemblyObjectIterator].GetType(currentComponent[1]);
					if (type != null)
					{
						MethodInfo[] listOfMethods = type.GetMethods(BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.DeclaredOnly);
						int methodObjectCount = listOfMethods.Length;
						for (int methodObjectIterator = 0; methodObjectIterator < methodObjectCount; ++methodObjectIterator)
						{
							string[] splittedString = listOfMethods[methodObjectIterator].ToString().Split(new Char[] { ' ', '(' });
							
							if (splittedString[1].Equals(methodName) == true)
							{
								Component currentObjectComponent = currentObject.GetComponent(currentComponent[1]);
								InvokeMethodAndSetValue(listOfMethods[methodObjectIterator], currentObjectComponent, inparameters);
								return;
							}
						}
					}
				}
			}
			
			Debug.LogError("Method was not found, or could not be accessed (method may be protected or private).");
			return;
		}
		
		/// <summary>
		/// Retrieves a component from the current GameObject which matches with the method inparameter.
		/// The method tries to find out which of the objects components the method belongs to and returns it.
		/// </summary>
		/// <param name="currentObject">The object the method is connected to.</param>
		/// <param name="methodName">The name of the method which needs to be found and invoked.</param>
		/// <param name="inparameters">Optional inparameters for the method.</param>
		private Component GetAssemblyComponent(GameObject currentObject, string methodName, object[] inparameters)
		{
			Component[] objectComponentList = currentObject.GetComponents(typeof(MonoBehaviour));
			int componentObjectCount = objectComponentList.Length;

			Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
			int referenceAssemblyObjectCount = referencedAssemblies.Length;

			for (int i = 0; i < componentObjectCount; ++i)
			{
				for (int assemblyObjectIterator = 0; assemblyObjectIterator < referenceAssemblyObjectCount; ++assemblyObjectIterator)
				{
					System.Type type = referencedAssemblies[assemblyObjectIterator].GetType(objectComponentList[i].GetType().ToString());
					if (type != null)
					{
						MethodInfo foundMethodObject = type.GetMethod(methodName);
						if(foundMethodObject != null)
							return currentObject.GetComponent(objectComponentList[i].GetType().ToString());
					}
				}
			}

			return null;
		}

		/// <summary>
		/// A method that invokes a method call. 
		/// This method is called by default when no other special methods can or will be called.
		/// </summary>
		/// <param name="currentMethodInformation">The current method which will be invoked (called).</param>
		/// <param name="currentComponent">The component the  method is attached to.</param>
		/// <param name="objectParameter">Inparameter object(s) which works as inparameters for the method call.</param>
		private void InvokeMethodAndSetValue(MethodInfo currentMethodInformation, Component currentComponent, object[] objectParameter)
		{
			try
			{
				currentMethodInformation.Invoke(currentComponent, objectParameter);
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

		/// <summary>
		/// Parses a string array and tries to set/get a value to the proper property.
		/// </summary>
		/// <param name="parsedObjectAndMethod">String array with the GameObject and its property.</param>
		/// <param name="parsedInparameterString">Optional parameters for the Set value method.</param>
		private bool ParseProperty(StringParser.ParsedInputParameterObject parameterObject)
		{
			object[] inparameterValue = m_stringParserObject.ParseParameterValue(parameterObject.Parameters);
			GameObject currentObject = parameterObject.InSceneGameObject;
			if (currentObject != null)
			{
				System.Type objectType = currentObject.GetType();
				PropertyInfo[] propertyArray = objectType.GetProperties();
				
				for (int i = 0; i < propertyArray.Length; ++i)
				{
					
					if (string.Equals(propertyArray[i].Name, parameterObject.Method) == true)
					{
						try
						{
							if (inparameterValue.Length <= 0)
							{
								object returnObjectValue = propertyArray[i].GetValue(currentObject, null);
								GuiConsoleWindow.Instance.AddReturnValue(returnObjectValue.ToString());
							}
							else
								propertyArray[i].SetValue(currentObject, inparameterValue[1], null);
						}
						catch (Exception e)
						{
							StringBuilder exceptionString = new StringBuilder();
							exceptionString.Append(e.Message);
							if (e.InnerException != null)
							{
								exceptionString.Append(" ");
								exceptionString.Append(e.InnerException.Message);
							}
							
							Debug.LogError(exceptionString.ToString());
						}
						
						return true;
					}
				}
			}
			else
				Debug.LogError("GameObject '" + parameterObject.InSceneGameObject.name + "' was not found.");
			
			return false;
		}

		/// <summary>
		/// The method parses a method call.
		/// This method is called if the GUI console window string is considered to be a method call.
		/// To be considered a method call, the GUI console window string needs to have a GameObject and a component.
		/// </summary>
		/// <param name="parsedObjectAndMethod">A string array containing the gameobject, component and method.</param>
		/// <param name="parsedInparameterString">A string array with the inparameter objects.</param>
		private bool ParseMethod(StringParser.ParsedInputParameterObject parameterObject)
		{
			StringBuilder newErrorString = new StringBuilder();
			object[] inparameterValue = m_stringParserObject.ParseParameterValue(parameterObject.Parameters);
			GameObject currentObject = parameterObject.InSceneGameObject;
			
			if (currentObject != null)
			{
				Component currentComponent = parameterObject.GameObjectComponent;
				
				if (currentComponent != null)
				{
					ParseComponentAndSendMethod(currentComponent, parameterObject.Method, inparameterValue);
					return true;
				}
				else
				{
					newErrorString.Append("Component '");
					newErrorString.Append(parameterObject.GameObjectComponent);
					newErrorString.Append("' was not found.");
					Debug.LogError(newErrorString.ToString());
				}
			}
			else
			{
				newErrorString.Append("Gameobject '");
				newErrorString.Append(parameterObject.InSceneGameObject);
				newErrorString.Append("' was not found.");
				Debug.LogError(newErrorString.ToString());
			}
			
			return false;
		}

		/// <summary>
		/// The method parses some string arrays to find the proper component, method and the needed inparameters.
		/// The method automatically checks if the method is a get or a set method.
		/// </summary>
		/// <param name="currentComponent">The current component the method is attached to.</param>
		/// <param name="parsedObjectAndMethod">A string array which stores the object and the method which will be called.</param>
		/// <param name="inparameterValue">A string array containing the inparameter values for the method call.</param>
		private void ParseComponentAndSendMethod(Component currentComponent, string methodName, object[] inparameterValue)
		{
			if (methodName == string.Empty)
			{
				Debug.LogError("No valid method was found.");
				return;
			}
			
			System.Type componentType = currentComponent.GetType();
			MethodInfo[] methodArray = componentType.GetMethods();
			
			for (int i = 0; i < methodArray.Length; ++i)
			{
				string[] name = methodArray[i].ToString().Split(' ');
				string[] newName = name[1].Split('(');
				if (string.Equals(newName[0], methodName) == true)
				{
					if (methodName.Contains("get") == true)
						InvokeMethodAndAddToConsole(methodArray[i], currentComponent);
					else
						InvokeMethodAndSetValue(methodArray[i], currentComponent, inparameterValue);
					
					return;
				}
			}
			
			Debug.LogError("No value was returned.");
		}

		/// <summary>
		/// A method that invokes a method call and returns the object value.
		/// This method is only called by method which has a "get_" string in the method name.
		/// The return value from the method is added to the Console window and the log file.
		/// </summary>
		/// <param name="currentMethodInformation">The current method which will be invoked (called).</param>
		/// <param name="currentComponent">The component the method is attached to.</param>
		private void InvokeMethodAndAddToConsole(MethodInfo currentMethodInformation, Component currentComponent)
		{
			object returnObject = currentMethodInformation.Invoke(currentComponent, null);
			
			if (returnObject != null)
				GuiConsoleWindow.Instance.AddReturnValue(returnObject.ToString());
			else
				Debug.LogError("No value was returned.");
		}

	}
}

/*
/// <summary>
	/// Tries to find the proper MethodInfo object based on the GameObject and a method string.
	/// </summary>
	private void GetMethodFromAssembly()
	{
		MethodInfo[] ListOFMethods = GetAssemblyMethodArray(OnPressRecieverObject);

		int objectCount = ListOFMethods.Length;
		for (int i = 0; i < objectCount; ++i)
		{
			if (ListOFMethods[i].Name.Equals(OnPressMethodName) == true)
			{
				m_methodInfoObject = ListOFMethods[i];
				m_componentObject = GetAssemblyComponent(OnPressRecieverObject, OnPressMethodName, null);
				m_methodInfoObject.Invoke(m_componentObject, null);
			}
		}
	}

	/// <summary>
	/// Adds all methods which is referenced to the current object.
	/// </summary>
	/// <param name="currentObject">The current object which the methods will be collected from.</param>
	/// <returns>An MethodInfo array with all added methods.</returns>
	private MethodInfo[] GetAssemblyMethodArray(GameObject currentObject)
	{
		List<MethodInfo> MethodList = new List<MethodInfo>();
		Component[] objectComponentList = currentObject.GetComponents(typeof(MonoBehaviour));
		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		int componentCount = objectComponentList.Length;
		int assemblyCount = referencedAssemblies.Length;

		for (int componentIndex = 0; componentIndex < componentCount; ++componentIndex)
		{
			for (int assemblyIndex = 0; assemblyIndex < assemblyCount; ++assemblyIndex)
			{
				System.Type type = referencedAssemblies[assemblyIndex].GetType(objectComponentList[componentIndex].GetType().ToString());
 
				if( type != null )
				{
					System.Reflection.MethodInfo []methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
					for (int methodIterator = 0; methodIterator < methods.Length; ++methodIterator)
						MethodList.Add(methods[methodIterator]);
				}
			}
		}

		return MethodList.ToArray();
	}

	/// <summary>
	/// Retrieves a component from the current GameObject which matches with the method inparameter.
	/// The method tries to find out which of the objects components the method belongs to and returns it.
	/// </summary>
	/// <param name="currentObject">The object the method is connected to.</param>
	/// <param name="methodName">The name of the method which needs to be found and invoked.</param>
	/// <param name="inparameters">Optional inparameters for the method.</param>
	private Component GetAssemblyComponent(GameObject currentObject, string methodName, object[] inparameters)
	{
		Component[] objectComponentList = currentObject.GetComponents(typeof(MonoBehaviour));
		int componentObjectCount = objectComponentList.Length;

		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		int referenceAssemblyObjectCount = referencedAssemblies.Length;

		for (int i = 0; i < componentObjectCount; ++i)
		{
			for (int assemblyObjectIterator = 0; assemblyObjectIterator < referenceAssemblyObjectCount; ++assemblyObjectIterator)
			{
				System.Type type = referencedAssemblies[assemblyObjectIterator].GetType(objectComponentList[i].GetType().ToString());
				if (type != null)
				{
					MethodInfo foundMethodObject = type.GetMethod(methodName);
					if(foundMethodObject != null)
						return currentObject.GetComponent(objectComponentList[i].GetType().ToString());
				}
			}
		}

		return null;
	}
*/