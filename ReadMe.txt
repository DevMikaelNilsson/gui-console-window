/*************************************************
The MIT License (MIT)
	
Copyright (c) 2014 Mikael "DevMikaelNilsson" Nilsson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*************************************************/

Console GUI Window

The console GUI window is a ingame, realtime 2D/GUI window which you can debug a game without the need of the Unity window.
In the console you can access all GameObjects which are currently active in the scene. By typing in the GameObject in the console GUI,
you get a list of all methods and properties you can access.
Access a GameObjects properties or methods from its components by typing in them in the Console window and press return.

Aliases are per scene, pre-defined, method calls. A alias is simply created in the LogManager component and can be accessed through the Console
GUI at any time. Access a alias by typing in the alias name and the optional parametrs. A alias object can be added through the LogManager component.

**** Console GUI Input ****

Clear All - Clears the whole Console GUI window. This does not affect the log.
Repeat last command - Rewrites the last input string written into the command window.

**** Alias commands: ****

[Alias name]
Starts the defined alias with that name.
    Ex. SetHealthValue
    Default setup for this Alias may look like this: 
    
    Alias Tag: SetHealthValue 
    Object: Player_1_GameObject 
    Message Type: [Can only be changed in the editor]
    Method: SetHealth
    Parameter: 100.0
    
    By calling this alias, the Player_1_GameObject will recieve a health of 100.0.

[Alias name] [parameter]
Starts the defined alias with that name, but overwrites the default parameter.
    Ex. SetHealthValue 125.0
    The customized setup for this Alias now look like this: 
    
    Alias Tag: SetHealthValue 
    Object: Player_1_GameObject 
    Message Type: [Can only be changed in the editor]
    Method: SetHealth 
    Parameter: 125.0

    The default value 100.0 is now overwritten with the new value 125.0. And the player will recieve 125.0 in health instad of 100.0.

[Alias name] [GameObject]
Starts the defined alias with that name, but overwrites the target GameObject.
    Ex. SetHealthValue Player_2_GameObject
    The customized setup for this Alias now look like this: 
    
    Alias Tag: SetHealthValue 
    Object: Player_2_GameObject 
    Message Type: [Can only be changed in the editor]
    Method: SetHealth 
    Parameter: 100.0

    The default GameObject Player_1_GameObject is now overwritten with the new value. 
    And the Player_2_GameObject will now recieve the 100.0 health instead of Player_1_GameObject.
    Do note that this will only work, as long as the Player_2_GameObject have the SetHealth method attached to it.

[Alias name] [GameObject] [parameter]
Starts the defined alias with that name, but overwrites the target GameObject AND the default parameter.
    Ex. SetHealthValue Player_2_GameObject 125.0
    The customized setup for this Alias now look like tis: 
    
    Alias Tag: SetHealthValue 
    Object: Player_2_GameObject 
    Message Type: [Can only be changed in the editor]
    Method: SetHealth 
    Parameter: 125.0

    The default GameObject Player_1_GameObject AND the default value 100.0 is now overwritten with the new values.
    And the Player_2_GameObject will now recieve 125.0 in health instead of the Player_1_GameObject recieving 100.0 in health.
    Do note that this will only work, as long as the Player_2_GameObject have the SetHealth method attached to it.

**** predefined commands: ****

SendMessage [GameObject] [Method] [(optional) Object]
Sends a message to a GameObject through the given method, with a optional parameter variable (object/float/bool/string etc.).
Do note that this will only work, as long as the GameObject have the method attached to it.

SendMessageUpwards [GameObject] [Method] [(optional) Object]
Sends a message upwards to a GameObject through the given method, with a optional parameter variable (object/float/bool/string etc.).
Do note that this will only work, as long as the GameObject have the method attached to it.

BroadcastMessage [GameObject] [(optional) Object]
Broadcasts a message to a GameObject through the given method, with a optional parameter variable (object/float/bool/string etc.).
Do note that this will only work, as long as the GameObject have the method attached to it.

DisplayObject [GameObject] [boolean]
Toggles a object and its children to be either visible (true) or invisible (false).
    Ex. Player_1_GameObject false

Clear
Clears the Console GUI window. Log is not affected.

Close
Closes the Console GUI window.

Exit
Closes the Application window.

LoadScene [int]
Loads a scene with the given index value.
    Ex. LoadScene 1

LoadScene [string]
Loads a scene with the given name string.
    Ex. LoadScene StartUp

FontSize [int]
Set the Console GUI skin font size. Default value is 12.
    Ex. FontSize 10

**** GUISkin settings: ****

The whole Console GUI is based on the Unity GUI skin system.
The Console window and its input window, are using the GUISkin Box type.
The popup window, which displays the available properties and methods, is using the GUISkin Window type.

These can be changed at any time in anyway you like to have them.